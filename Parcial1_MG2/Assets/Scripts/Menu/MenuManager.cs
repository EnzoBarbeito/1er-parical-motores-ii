using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    #region -----Variables-----
    private short decoy;
    [Header("--- Managers --- \n")]
    [SerializeField] private AudioMenu AdMenu;
    [SerializeField] private AudioSource SFX;
    [SerializeField] private float timer;
    [Header("--- Men�s --- \n")]
    [SerializeField] internal GameObject Video;
    [SerializeField] internal GameObject IntroScreen;
    [SerializeField] internal GameObject MenuP;
    [SerializeField] internal GameObject Rockola;
    [SerializeField] internal GameObject Creditos;
    [SerializeField] internal GameObject Controles;
    [SerializeField] internal GameObject Jugar;
    [Header("--- Rockola --- \n")]
    [SerializeField] internal GameObject Pausa;
    [SerializeField] internal GameObject Reproducir;
    [SerializeField] private TMPro.TMP_Text nombreCancion;
    [SerializeField] internal GameObject Lyrics_Fit;
    [SerializeField] internal GameObject Lyrics_Rich;
    [SerializeField] internal GameObject Lyrics_Iya;
    #endregion

    void Start()
    {
        StartCoroutine(CutsceneTime());
        GameObject adm = GameObject.Find("-- Audio Manager --");
        GameObject sfx = GameObject.Find("-- SFX Manager --");
        SFX = sfx.GetComponent<AudioSource>();
        AdMenu = adm.GetComponent<AudioMenu>();
        Lyrics_Fit.SetActive(false);
        Lyrics_Rich.SetActive(false);
        Lyrics_Iya.SetActive(false);
        timer = 0;
    }
    void Update()
    {
        GetInput();
        timer = timer + 1 * Time.deltaTime;
    }
    public void Cutscene()
    {
        Video.SetActive(true);
        IntroScreen.SetActive(false);
        MenuP.SetActive(false);
        Rockola.SetActive(false);
        Creditos.SetActive(false);
        Controles.SetActive(false);
        Jugar.SetActive(false);

    }
    public void Intro()
    {
        Cursor.visible = true;
        Video.SetActive(false);
        IntroScreen.SetActive(true);
        MenuP.SetActive(false);
        Rockola.SetActive(false);
        Creditos.SetActive(false);
        Controles.SetActive(false);
        Jugar.SetActive(false);
    }
    public void MenuPrincipal()
    {
        Video.SetActive(false);
        IntroScreen.SetActive(false);
        MenuP.SetActive(true);
        Rockola.SetActive(false);
        Creditos.SetActive(false);
        Controles.SetActive(false);
        Jugar.SetActive(false);
        Lyrics_Fit.SetActive(false);
        Lyrics_Iya.SetActive(false);
        Lyrics_Rich.SetActive(false);
    }
    public void rockola()
    {
        Video.SetActive(false);
        IntroScreen.SetActive(false);
        MenuP.SetActive(false);
        Rockola.SetActive(true);
        Creditos.SetActive(false);
        Controles.SetActive(false);
        Jugar.SetActive(false);
    }
    public void creditos()
    {
        Video.SetActive(false);
        IntroScreen.SetActive(false);
        MenuP.SetActive(false);
        Rockola.SetActive(false);
        Creditos.SetActive(true);
        Controles.SetActive(false);
        Jugar.SetActive(false);
    }
    public void controles()
    {
        Video.SetActive(false);
        IntroScreen.SetActive(false);
        MenuP.SetActive(false);
        Rockola.SetActive(false);
        Creditos.SetActive(false);
        Controles.SetActive(true);
        Jugar.SetActive(false);
    }
    public void jugar()
    {
        Video.SetActive(false);
        IntroScreen.SetActive(false);
        MenuP.SetActive(false);
        Rockola.SetActive(false);
        Creditos.SetActive(false);
        Controles.SetActive(false);
        Jugar.SetActive(true);
    }
    public void Salir()
    {
        Application.Quit();
    }
    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && Video.activeInHierarchy==true)
        {
            StopAllCoroutines();
            Intro();

        }
        if (Input.GetKeyDown(KeyCode.Space) && IntroScreen.activeInHierarchy == true)
        {
            MenuPrincipal();

        }
        if (Input.GetKeyDown(KeyCode.Escape) && Rockola.activeInHierarchy == true)
        {
            MenuPrincipal();

        }
        if (Input.GetKeyDown(KeyCode.Escape) && Creditos.activeInHierarchy == true)
        {
            MenuPrincipal();

        }
        if (Input.GetKeyDown(KeyCode.Escape) && Jugar.activeInHierarchy == true)
        {
            MenuPrincipal();

        }
        if (Input.GetKeyDown(KeyCode.Escape) && (Lyrics_Fit.activeInHierarchy == true || Lyrics_Iya.activeInHierarchy == true | Lyrics_Rich.activeInHierarchy == true))
        {
            MenuPrincipal();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
        }
    }
    public void ReproducirCancion()
    {
        AdMenu.musica.Play();
        Reproducir.SetActive(false);
        Pausa.SetActive(true);
        AdMenu.EstaSondandoFull = true;

    }
    public void PausarCancion()
    {
        AdMenu.musica.Pause();
        Pausa.SetActive(false);
        Reproducir.SetActive(true);
        AdMenu.EstaSondandoFull = false;

    }
    public void SiguienteCancion()
    {
        

        if (nombreCancion.text == "Rich Personality")
        {
            AdMenu.musica.clip = AdMenu.FullSongs[1];
            nombreCancion.text = "In Your Arms";

        }
        else if (nombreCancion.text == "In Your Arms")
        {
            AdMenu.musica.clip = AdMenu.FullSongs[2];
            nombreCancion.text = "Fit Your Heart";

        }
        else if (nombreCancion.text == "Fit Your Heart")
        {
            AdMenu.musica.clip = AdMenu.FullSongs[0];
            nombreCancion.text = "Rich Personality";

        }
        AdMenu.GetComponent<AudioHighPassFilter>().enabled = false;

    }
    public void AnteriorCancion()
    {
        if (nombreCancion.text == "Rich Personality")
        {
            AdMenu.musica.clip = AdMenu.FullSongs[2];
            nombreCancion.text = "Fit Your Heart";

        }
        else if (nombreCancion.text == "Fit Your Heart")
        {
            AdMenu.musica.clip = AdMenu.FullSongs[1];
            nombreCancion.text = "In Your Arms";

        }
        else if (nombreCancion.text == "In Your Arms")
        {
            AdMenu.musica.clip = AdMenu.FullSongs[0];
            nombreCancion.text = "Rich Personality";

        }
        AdMenu.GetComponent<AudioHighPassFilter>().enabled = false;

    }
    public void Lyrics()
    {
        if (nombreCancion.text=="Rich Personality")
        {
            Lyrics_Rich.SetActive(true);
        }
        if (nombreCancion.text == "In Your Arms")
        {
            Lyrics_Iya.SetActive(true);
        }
        if (nombreCancion.text == "Fit Your Heart")
        {
            Lyrics_Fit.SetActive(true);
        }
    }

    public void Entendido()
    {
        SceneManager.LoadScene(1);
    }
    IEnumerator CutsceneTime()
    {
        Cursor.visible = false;
        Video.SetActive(true);
        IntroScreen.SetActive(false);
        MenuP.SetActive(false);
        Rockola.SetActive(false);
        Creditos.SetActive(false);
        Controles.SetActive(false);
        Jugar.SetActive(false);
        yield return new WaitForSecondsRealtime(10f);
        Intro();
        yield break;
    }
}
