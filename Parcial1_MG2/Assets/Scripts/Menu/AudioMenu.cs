using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMenu : MonoBehaviour
{
    private short decoy;
    [Header("--- Variables Generales--- \n")]
    [SerializeField] internal MenuManager menu;
    [SerializeField] internal AudioSource musica;
    [SerializeField] internal AudioClip[] ShortSong;
    [SerializeField] internal AudioClip[] FullSongs;
    [Header("--- Menu Checks--- \n")]
    [SerializeField] internal bool EstaSonandoC1;
    [SerializeField] internal bool EstaSonandoC2;
    [SerializeField] internal bool EstaSonandoC3;
    [SerializeField] internal bool EstaSonandoC5;
    [SerializeField] internal bool EstaSonandoRockola;
    [SerializeField] internal bool EstaSondandoFull;



    void Start()
    {
        EstaSonandoC1 = false;
        EstaSonandoC2 = false;
        EstaSonandoC3 = false;
        EstaSonandoC5 = false;
        EstaSonandoRockola = false;
        EstaSondandoFull = false;
        GameObject m = GameObject.Find("-- Menu Manager --");
        menu = m.GetComponent<MenuManager>();
        musica = gameObject.GetComponent<AudioSource>();
        musica.loop = true;
        musica.volume=1;
        musica.playOnAwake = true;
        

    }

    // Update is called once per frame
    void Update()
    {
        ReproducirC1(); //Cutscene/Intro
        ReproducirC2(); //MenuP
        ReproducirC3(); //Jugar
        ReproducirC4(); //Rockola
        ReproducirC5(); //Creditos
        SacarEfectoRockola();

    }


    private void ReproducirC1()
    {
        if((menu.Video.activeInHierarchy==true && EstaSonandoC1 == false || menu.IntroScreen.activeInHierarchy == true && EstaSonandoC1==false) && EstaSondandoFull==false)
        {
            musica.clip = ShortSong[0];
            musica.Play();
            EstaSonandoC1 = true;
            EstaSonandoC2 = false;
            EstaSonandoC3 = false;
            EstaSonandoRockola = false;

        }
    }
    private void ReproducirC2()
    {
        if ((menu.MenuP.activeInHierarchy == true && EstaSonandoC2 == false) && EstaSondandoFull==false)
        {
            musica.clip = ShortSong[1];
            musica.Play();
            EstaSonandoC1 = false;
            EstaSonandoC2 = true;
            EstaSonandoC3 = false;
            EstaSonandoC5 = false;
            EstaSonandoRockola = false;

        }
    }
    private void ReproducirC3()
    {
        if ((menu.Jugar.activeInHierarchy == true && EstaSonandoC3 == false) && EstaSondandoFull==false)
        {
            musica.clip = ShortSong[2];
            musica.Play();
            EstaSonandoC1 = false;
            EstaSonandoC2 = false;
            EstaSonandoC3 = true;
            EstaSonandoC5 = false;
            EstaSonandoRockola = false;

        }
    }
    private void ReproducirC4()
    {
        if ((menu.Rockola.activeInHierarchy == true && EstaSonandoRockola==false)&& EstaSondandoFull==false)
        {
           
            gameObject.AddComponent<AudioHighPassFilter>();
            AudioHighPassFilter tommy = gameObject.GetComponent<AudioHighPassFilter>();
            tommy.cutoffFrequency = 300;
            tommy.highpassResonanceQ = 10;
            if (gameObject.GetComponent<AudioHighPassFilter>().enabled == false)
            {
                gameObject.GetComponent<AudioHighPassFilter>().enabled = true;

            }
            EstaSonandoRockola = true;

        }
    }

    private void ReproducirC5()
    {
        if((menu.Creditos.activeInHierarchy==true && EstaSonandoC5 == false) && EstaSondandoFull== false)
        {
            musica.clip = ShortSong[3];
            musica.Play();
            EstaSonandoC1 = false;
            EstaSonandoC2 = false;
            EstaSonandoC3 = false;
            EstaSonandoRockola = false;
            EstaSonandoC5 = true;
        }
    }

    private void SacarEfectoRockola()
    {
        if (EstaSonandoRockola == true && menu.Rockola.activeInHierarchy==false)
        {
            gameObject.GetComponent<AudioHighPassFilter>().enabled = false;
            EstaSonandoRockola = false;
        }
    }


}
