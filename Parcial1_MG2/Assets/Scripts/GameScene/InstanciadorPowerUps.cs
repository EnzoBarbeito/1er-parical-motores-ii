using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorPowerUps : MonoBehaviour
{
    [SerializeField] private Rigidbody lifeup;
    [SerializeField] private Rigidbody moneda;
    [SerializeField] private Rigidbody STS;
    [SerializeField] private float fuerza;
    [SerializeField] private GameManager Manager;
    void Start()
    {
        STS= GameObject.Find("STS").GetComponent<Rigidbody>();
        moneda = GameObject.Find("Moneda").GetComponent<Rigidbody>();
        lifeup = GameObject.Find("Life-Up").GetComponent<Rigidbody>();
        Manager = GameObject.Find("-- Game Manager --").GetComponent<GameManager>();
    }
    private void Instanciar1UP()
    {

        var clone = Instantiate(lifeup, gameObject.transform.position, lifeup.transform.rotation);
        clone.AddForce(Vector3.down * fuerza, ForceMode.Impulse);

    }

    public void InstanciarMoneda()
    {
        var clone = Instantiate(moneda, moneda.transform.parent.position, moneda.transform.rotation);
        clone.AddForce(Vector3.down * fuerza, ForceMode.Impulse);
    }
    public void InstanciarSTS()
    {
        var clone = Instantiate(STS, STS.transform.parent.position, STS.transform.rotation);
        clone.AddForce(Vector3.down * fuerza, ForceMode.Impulse);
    }

    internal IEnumerator VidaExtra()
    {
        Manager.corutinaActivada = true;
        Debug.Log("SE EJECUTO LA CORRUTINA DE VIDA EXTRA");
        yield return new WaitForSecondsRealtime(20f);
        Instanciar1UP();
        while(Manager.vidasJugador<=6 && Manager.vidasJugador >= 4)
        {
            yield return new WaitForSecondsRealtime(20f);
            Instanciar1UP();
        }
        while (Manager.vidasJugador <= 3 && Manager.vidasJugador >= 1)
        {
            yield return new WaitForSecondsRealtime(10f);
            Instanciar1UP();
        }
        yield break;
    }


}
