using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    private short decoy;
    [Header("--- Moviemiento B�sico ---\n")]
    [SerializeField] private float Horizontal;
    [SerializeField] private float Vertical;
    [SerializeField] private Rigidbody proyectil;
    [SerializeField] private float velocidad;
    [SerializeField] private GameManager Manager;
    [SerializeField] private bool noPuedeAvanzarDer;
    [SerializeField] private bool noPuedeAvanzarUp;
    [SerializeField] private bool noPuedeAvanzarDown;
    [SerializeField] private bool noPuedeAvanzarIzq;
    [Header("--- Mecanica Disparo ---\n")]
    [SerializeField] internal bool Hypercharge;
    [SerializeField] internal bool triShoot;
    [SerializeField] private float velocidadCadencia;
    [SerializeField] private float velocidadCadenciaDisparo;
    [SerializeField] internal bool puedeDisparar;
    [Header("--- Varibales TEST ---\n")]
    [SerializeField] private POWERUP2 pow2;
    [SerializeField] private float Rotacion_en_Z;
    [SerializeField] private float Posicion_en_Y;
    [SerializeField] private float Posicion_en_X;
    

    void Start()
    {
        pow2 = GameObject.Find("InstanciadorEnemigosUpper").GetComponent<POWERUP2>();
        GameObject m = GameObject.Find("-- Game Manager --");
        Manager = m.GetComponent<GameManager>();
        GameObject p = GameObject.Find("Proyectil");
        proyectil = p.GetComponent<Rigidbody>();
        noPuedeAvanzarDer = false;
        noPuedeAvanzarIzq = false;
        noPuedeAvanzarUp = false;
        noPuedeAvanzarDown = false;
        triShoot = false;
        puedeDisparar = true;
        Hypercharge = false;
        velocidadCadencia = 0.1f;


    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        HUD();
        EliminarProyectiles();
        MovimientoJugador();
        CambiosCadencia();
        Rotacion_en_Z = transform.rotation.z;
        Posicion_en_X = transform.position.x;
        Posicion_en_Y = transform.position.y;
    }

    private void GetInput()
    {
        if ((Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) && noPuedeAvanzarDer == false)
        {
           
            Vector3 posicion = transform.position + new Vector3(+Horizontal, 0, 0);
            transform.position = posicion;
            //GDA.instancia.ReproducirSonido("Z2");

        }
        if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))&& noPuedeAvanzarIzq ==false)
        {
            Vector3 posicion = transform.position + new Vector3(-Horizontal, 0, 0);
            transform.position = posicion;
            //GDA.instancia.ReproducirSonido("Z3");

        }
        if ((Input.GetKeyDown(KeyCode.W)|| Input.GetKeyDown(KeyCode.UpArrow))&& noPuedeAvanzarUp == false)
        {
            Vector3 posicion = transform.position + new Vector3(0, Vertical, 0);
            transform.position = posicion;
            //GDA.instancia.ReproducirSonido("Z4");

        }
        if ((Input.GetKeyDown(KeyCode.S)|| Input.GetKeyDown(KeyCode.DownArrow)) && noPuedeAvanzarDown == false)
        {
            Vector3 posicion = transform.position + new Vector3(0, -Vertical, 0);
            transform.position = posicion;
            //GDA.instancia.ReproducirSonido("Z1");

        }
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.X)) && triShoot == false && puedeDisparar==true && Hypercharge==false)
        {
            puedeDisparar = false;
            if (gameObject.transform.rotation.z== 0)
            {
                Debug.Log("Esta en 0");
                var clone = Instantiate(proyectil, transform.position, transform.rotation);
                clone.AddForce(Vector3.right * velocidad, ForceMode.Impulse);

            }
            if (gameObject.transform.rotation.z == 1 || gameObject.transform.rotation.z == -1)  
            {
                Debug.Log("Esta en 180");
                var clone = Instantiate(proyectil, transform.position, transform.rotation);
                clone.AddForce(Vector3.left * velocidad, ForceMode.Impulse);
            }
            GDA.instancia.ReproducirSonido("Punch");
            StartCoroutine(tiempoCadencia());
        }
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.X)) && triShoot == true && Hypercharge==false)
        {
            StartCoroutine(tripleTiro());

        }
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.X)) && triShoot == false && Hypercharge == true)
        {
            StartCoroutine(TiroAutomatico());

        }
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Z))
        {
            transform.Rotate(Vector3.forward, 180f);
            //GDA.instancia.ReproducirSonido("ChangeL");

        }
        if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.C))
        {
            transform.Rotate(Vector3.forward, -180f);
            //GDA.instancia.ReproducirSonido("ChangeR");

        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(1);
        }

        //DEBUG ONLY//
        //---------------------------------------------------//
        if (Input.GetKeyDown(KeyCode.O))
        {
            //Manager.vidasJugador-=1;
            //pow2.InstanciarSTS();

        } 
        if (Input.GetKeyDown(KeyCode.P))
        {
            Manager.vidasJugador += 1;

        }
        //--------------------------------------------------//
        //DEBUG ONLY
    }

    private void EliminarProyectiles()
    {
        GameObject[] proyectiles = GameObject.FindGameObjectsWithTag("Proyectil");
        foreach(GameObject b in proyectiles)
        {
            Destroy(b, 3f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Coleccionable") == true)
        {
            Manager.Coleccionables += 1;
            Destroy(collision.gameObject);
        }
    }

    private void MovimientoJugador()
    {
        if (transform.position.x == 3.46f && (transform.position.y == 6.9f || transform.position.y == 3.4f || transform.position.y <= -0.0999999f))
        {
            noPuedeAvanzarDer = true;
        }
        else
        {
            noPuedeAvanzarDer = false;
        }
        if (transform.position.x <= -3.54f && (transform.position.y == 6.9f || transform.position.y == 3.4f || transform.position.y <= -0.0999999f))
        {
            noPuedeAvanzarIzq = true;
        }
        else
        {
            noPuedeAvanzarIzq = false;

        }
        if (transform.position.y == 6.9f && (transform.position.x == -3.54f || transform.position.x == 3.46f || transform.position.x <= -0.03999996f))
        {
            noPuedeAvanzarUp = true;
        }
        else
        {
            noPuedeAvanzarUp = false;

        }
        if (transform.position.y <= -0.0999999f && (transform.position.x == -3.54f || transform.position.x == 3.46f || transform.position.x <= -0.03999996f))
        {
            noPuedeAvanzarDown = true;
        }
        else
        {
            noPuedeAvanzarDown = false;

        }
    }

    IEnumerator tripleTiro()
    {
        if ((gameObject.transform.rotation.z == 0) && puedeDisparar==true)
        {
            puedeDisparar = false;
            var clone1 = Instantiate(proyectil, transform.position, transform.rotation);
            clone1.AddForce(Vector3.right * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            var clone2 = Instantiate(proyectil, transform.position, transform.rotation);
            clone2.AddForce(Vector3.right * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            var clone3 = Instantiate(proyectil, transform.position, transform.rotation);
            clone3.AddForce(Vector3.right * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            StartCoroutine(tiempoCadencia());
            yield break;
        }
        if ((gameObject.transform.rotation.z == 1 || gameObject.transform.rotation.z == -1) && puedeDisparar==true)
        {
            puedeDisparar = false;
            var clone1 = Instantiate(proyectil, transform.position, transform.rotation);
            clone1.AddForce(Vector3.left * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            var clone2 = Instantiate(proyectil, transform.position, transform.rotation);
            clone2.AddForce(Vector3.left * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            var clone3 = Instantiate(proyectil, transform.position, transform.rotation);
            clone3.AddForce(Vector3.left * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            StartCoroutine(tiempoCadencia());
            yield break;
        }

    }

    IEnumerator tiempoCadencia()
    {
        Debug.Log("Se ejecuto la corrutina tiempoCadencia");
        yield return new WaitForSecondsRealtime(velocidadCadenciaDisparo);
        puedeDisparar = true;
        yield break;
    }

    private void CambiosCadencia()
    {
        if (triShoot == true)
        {
            velocidadCadenciaDisparo = 0.6f;
        }
        else if (triShoot == false)
        {
            velocidadCadenciaDisparo = 0.4f;
        }

    }

    IEnumerator TiroAutomatico()
    {
        if (gameObject.transform.rotation.z == 0)
        {
            var clone1 = Instantiate(proyectil, transform.position, transform.rotation);
            clone1.AddForce(Vector3.right * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            var clone2 = Instantiate(proyectil, transform.position, transform.rotation);
            clone2.AddForce(Vector3.right * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            yield break;
        }
        if (gameObject.transform.rotation.z == 1 || gameObject.transform.rotation.z == -1)
        {
            puedeDisparar = false;
            var clone1 = Instantiate(proyectil, transform.position, transform.rotation);
            clone1.AddForce(Vector3.left * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            var clone2 = Instantiate(proyectil, transform.position, transform.rotation);
            clone2.AddForce(Vector3.left * velocidad, ForceMode.Impulse);
            GDA.instancia.ReproducirSonido("Punch");
            yield return new WaitForSecondsRealtime(velocidadCadencia);
            yield break;
        }

    }

    private void HUD()
    {
        if (triShoot == false && Hypercharge == false)
        {
            Manager.elementosHUD[3].gameObject.SetActive(true);
            Manager.elementosHUD[4].gameObject.SetActive(false);
            Manager.elementosHUD[5].gameObject.SetActive(false);
        }
        if (triShoot == true)
        {
            Manager.elementosHUD[3].gameObject.SetActive(false);
            Manager.elementosHUD[4].gameObject.SetActive(true);
            Manager.elementosHUD[5].gameObject.SetActive(false);
        }
        if (Hypercharge== true)
        {
            Manager.elementosHUD[3].gameObject.SetActive(false);
            Manager.elementosHUD[4].gameObject.SetActive(false);
            Manager.elementosHUD[5].gameObject.SetActive(true);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("EnemigoAereo") == true)
        {
            Manager.vidasJugador -= 1;
            Destroy(other.gameObject);
        }
    }

}
