using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrillaCollider : MonoBehaviour
{
    [SerializeField] private GameManager Manager;
    
    void Start()
    {
        GameObject m = GameObject.Find("-- Game Manager --");
        Manager = m.GetComponent<GameManager>();
    }

    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Manager.vidasJugador -= 1;
            if (Manager.vidasJugador == 3 || Manager.vidasJugador == 6)
            {
                GDA.instancia.ReproducirSonido("DAMAGE");

            }
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            Manager.vidasJugador -= 1;
            if (Manager.vidasJugador == 3 || Manager.vidasJugador == 6)
            {
                GDA.instancia.ReproducirSonido("DAMAGE");

            }
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("EnemigoBIG"))
        {
            EnemigoBIG a = other.gameObject.GetComponent<EnemigoBIG>();
            if (a.contadorHits == 0)
            {
                Manager.vidasJugador -= 1;
            }
            if (a.contadorHits == 1)
            {
                Manager.vidasJugador -= 2;
            }
            if (a.contadorHits == 2)
            {
                Manager.vidasJugador -= 3;
            }
            if (Manager.vidasJugador == 3 || Manager.vidasJugador == 6)
            {
                GDA.instancia.ReproducirSonido("DAMAGE");

            }
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("EnemigoGHOST"))
        {
            Manager.vidasJugador -= 1;
            Destroy(other.gameObject);
            if (Manager.vidasJugador == 3 || Manager.vidasJugador == 6)
            {
                GDA.instancia.ReproducirSonido("DAMAGE");

            }

        }
    }
}
