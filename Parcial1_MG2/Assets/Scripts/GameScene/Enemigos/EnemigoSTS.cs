using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoSTS : MonoBehaviour
{
    [SerializeField] private Transform enemigo;
    [SerializeField] private bool check;
    void Start()
    {
        enemigo = GameObject.Find("STS").GetComponent<Transform>();
    }

    void Update()
    {
        if (check == false)
        {
            StartCoroutine(SideToSide());
        }
    }

    IEnumerator SideToSide()
    {
        check = true;
        enemigo.position = new Vector3(-3.54f, enemigo.position.y, enemigo.position.z);
        yield return new WaitForSecondsRealtime(0.5f);
        enemigo.position = new Vector3(-0.04000092f, enemigo.position.y, enemigo.position.z);
        yield return new WaitForSecondsRealtime(0.5f);
        enemigo.position = new Vector3(3.46f, enemigo.position.y, enemigo.position.z);
        yield return new WaitForSecondsRealtime(0.5f);
        check = false;
        yield break;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pared"))
        {
            Destroy(gameObject);
        }
    }
}
