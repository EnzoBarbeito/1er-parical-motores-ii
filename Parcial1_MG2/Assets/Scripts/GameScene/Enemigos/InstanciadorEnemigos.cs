using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorEnemigos : MonoBehaviour
{
    [SerializeField] private Rigidbody enemigoComun;
    [SerializeField] private Rigidbody enemigoBIG;
    [SerializeField] private Rigidbody enemigoGHOST;
    [SerializeField] private float fuerza;
    [SerializeField] private ControlJugador jugador;
    [SerializeField] private GameObject letra;
    [SerializeField] private GameManager Manager;
    [SerializeField] private POWERUP2 powerUps;

    void Start()
    {
        powerUps = GameObject.Find("Instanciador Modificadores").GetComponent<POWERUP2>();
        jugador = GameObject.Find("Jugador").GetComponent<ControlJugador>();
        enemigoComun = GameObject.Find("EnemigoDer").GetComponent<Rigidbody>();
        enemigoBIG= GameObject.Find("EnemigoBIG").GetComponent<Rigidbody>();
        enemigoGHOST = GameObject.Find("EnemigoGHOST").GetComponent<Rigidbody>();
        Manager = GameObject.Find("-- Game Manager --").GetComponent<GameManager>();
        letra= Manager.elementosHUD[15];
        letra.gameObject.SetActive(false);
        StartCoroutine(LyricsTest());
        StartCoroutine(ACTO1());
        StartCoroutine(CorutinaPruebaLOOP());
        StartCoroutine(Corutina1stBOOM());
        StartCoroutine(BackToNormal());
        StartCoroutine(HyperMode1());
        StartCoroutine(BackToNormal2());
        StartCoroutine(Triple2());
        StartCoroutine(HyperMode2());
    }
    void Update()
    {
        GetInput();
    }
    private void GetInput()
    {

        if (Input.GetKeyDown(KeyCode.L) && (gameObject.name=="Instanciador A-1" || gameObject.name == "Instanciador A-2" || gameObject.name == "Instanciador A-3"))
        {
            var clone = Instantiate(enemigoComun, transform.position, transform.rotation);
            clone.AddForce(Vector3.left * fuerza, ForceMode.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.L) && (gameObject.name == "Instanciador B-1"|| gameObject.name == "Instanciador B-2" || gameObject.name == "Instanciador B-3"))
        {
            var clone = Instantiate(enemigoComun, transform.position, transform.rotation);
            clone.AddForce(Vector3.right * fuerza, ForceMode.Impulse);
        }
    }

    #region -----Instanciadores-----
    private void InstanciarA1(Rigidbody enemigo)
    {
        if (gameObject.name == "Instanciador A-1")
        {
            var clone = Instantiate(enemigo, transform.position, transform.rotation);
            clone.AddForce(Vector3.left * fuerza, ForceMode.Impulse);
        }

    }
    private void InstanciarA2(Rigidbody enemigo)
    {
        if (gameObject.name == "Instanciador A-2")
        {
            var clone = Instantiate(enemigo, transform.position, transform.rotation);
            clone.AddForce(Vector3.left * fuerza, ForceMode.Impulse);
        }

    }
    private void InstanciarA3(Rigidbody enemigo)
    {
        if (gameObject.name == "Instanciador A-3")
        {
            var clone = Instantiate(enemigo, transform.position, transform.rotation);
            clone.AddForce(Vector3.left * fuerza, ForceMode.Impulse);
        }

    }
    private void InstanciarB1(Rigidbody enemigo)
    {
        if (gameObject.name == "Instanciador B-1")
        {
            var clone = Instantiate(enemigo, transform.position, transform.rotation);
            clone.AddForce(Vector3.right * fuerza, ForceMode.Impulse);
        }

    }
    private void InstanciarB2(Rigidbody enemigo)
    {
        if (gameObject.name == "Instanciador B-2")
        {
            var clone = Instantiate(enemigo, transform.position, transform.rotation);
            clone.AddForce(Vector3.right * fuerza, ForceMode.Impulse);
        }

    }
    private void InstanciarB3(Rigidbody enemigo)
    {
        if (gameObject.name == "Instanciador B-3")
        {
            var clone = Instantiate(enemigo, transform.position, transform.rotation);
            clone.AddForce(Vector3.right * fuerza, ForceMode.Impulse);
        }

    }
    #endregion

    #region Corutinas
    IEnumerator ACTO1() // Comienza en 21 Segundos y finaliza en 67 segundos de la cancion.
    {
        yield return new WaitForSecondsRealtime(21f); // Comienzo en 21.
        Debug.Log("PASARON 21 SEGUNDOS");
        for (int i = 0; i < 5; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        //yield return new WaitForSecondsRealtime(0.5f); // Entrada a 25 segundos.
        for (int i = 0; i < 5; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        //yield return new WaitForSecondsRealtime(0.5f); // Entrada a 30 segundos.
        for (int i = 0; i < 5; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        powerUps.InstanciarMoneda();
        //yield return new WaitForSecondsRealtime(0.5f); // Entrada a 35 segundos.
        for (int i = 0; i < 5; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        //yield return new WaitForSecondsRealtime(1.5f); // Entrada a 40 segundos.
        for (int i = 0; i < 5; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(1f);
        for (int i = 0; i < 7; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i <= 1; i++)
        {
            powerUps.InstanciarSTS();
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);

        }
        for (int i = 0; i <= 1; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);

        }
        for (int i = 0; i <= 1; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);

        }
        for (int i = 0; i <= 1; i++)
        {

            powerUps.InstanciarSTS();
            
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);

        }
        for (int i = 0; i <= 1; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);

        }
        for (int i = 0; i <= 1; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i <= 5; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        yield break;
    }
    IEnumerator CorutinaPruebaLOOP()
    {
        yield return new WaitForSecondsRealtime(51.5f);
        Debug.Log("Entrando en LOOP");
        for (int i = 0; i < 15; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        Debug.Log("Entrando en LOOP");
        for (int i = 0; i < 14; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }

        yield break;
    }
    IEnumerator Corutina1stBOOM()
    {
        yield return new WaitForSecondsRealtime(67.5f);
        Debug.Log("Entrando en 1ST BOOM");
        GDA.instancia.ReproducirSonido("TRIPLE");
        jugador.triShoot = true;
        for (int i = 0; i < 3; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 3; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 3; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 3; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);


        InstanciarB3(enemigoBIG);
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 5; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }

        for (int i = 0; i < 5; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 3; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }

        for (int i = 0; i < 3; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 3; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        for (int i = 0; i < 3; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        for (int i = 0; i < 3; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        yield break;
    }
    IEnumerator BackToNormal()
    {
        yield return new WaitForSecondsRealtime(115f);
        jugador.triShoot = false;
        jugador.Hypercharge = false;
        yield return new WaitForSecondsRealtime(1f);
        InstanciarB1(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB3(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB1(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB3(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB1(enemigoComun);
        powerUps.InstanciarMoneda();
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB2(enemigoComun);
        yield return new WaitForSecondsRealtime(1f);
        InstanciarA1(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarA2(enemigoGHOST);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarA3(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarA2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarA1(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarA2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarA3(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarA2(enemigoGHOST);
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 5; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        InstanciarA2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        InstanciarA2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 6; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 4; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 6; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        InstanciarB2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB3(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        InstanciarB2(enemigoComun);
        yield return new WaitForSecondsRealtime(0.5f);
        yield break;
    }
    IEnumerator HyperMode1()
    {
        yield return new WaitForSecondsRealtime(143f);
        jugador.Hypercharge = true;
        jugador.triShoot = false;
        GDA.instancia.ReproducirSonido("HYPER");
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 4; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 4; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }


        yield break;
    }
    IEnumerator BackToNormal2()
    {
        yield return new WaitForSecondsRealtime(176f);
        jugador.puedeDisparar = true;
        jugador.triShoot = false;
        jugador.Hypercharge = false;
        for (int i = 0; i < 5; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 5; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield break;
    }
    IEnumerator Triple2()
    {
        yield return new WaitForSecondsRealtime(190f);
        GDA.instancia.ReproducirSonido("TRIPLE");
        jugador.triShoot = true;
        jugador.Hypercharge = false;
        InstanciarB2(enemigoBIG);
        yield return new WaitForSecondsRealtime(1.5f);
        InstanciarB3(enemigoBIG);
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 3; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < 3; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.3f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        yield break;
    }
    IEnumerator HyperMode2()
    {
        yield return new WaitForSecondsRealtime(205f);
        jugador.Hypercharge = true;
        jugador.triShoot = false;
        GDA.instancia.ReproducirSonido("HYPER");
        yield return new WaitForSecondsRealtime(0.3f);
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        powerUps.InstanciarSTS();

        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarA1(enemigoBIG);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        powerUps.InstanciarSTS();
        powerUps.InstanciarMoneda();
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoBIG);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        powerUps.InstanciarSTS();

        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoBIG);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return new WaitForSecondsRealtime(0.5f);
        powerUps.InstanciarSTS();

        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        powerUps.InstanciarSTS();
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoBIG);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        powerUps.InstanciarSTS();
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 4; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoBIG);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        powerUps.InstanciarSTS();
        for (int i = 0; i < 2; i++)
        {
            InstanciarA3(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB2(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarB1(enemigoBIG);
            yield return new WaitForSecondsRealtime(0.5f);
        }
        for (int i = 0; i < 2; i++)
        {
            InstanciarA1(enemigoComun);
            yield return new WaitForSecondsRealtime(0.5f);
        }

        InstanciarA2(enemigoGHOST);
        yield return new WaitForSecondsRealtime(0.5f);


        yield break;
    }
    IEnumerator LyricsTest()
    {
        yield return new WaitForSecondsRealtime(67f); //Entra exactamente en 66.8 de timer Diferencia total de 0.2, restarle este valor al timer en codigo.
        letra.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(1.6f); // Destino final perfecto a 68.4.
        letra.gameObject.SetActive(false);
        yield return new WaitForSecondsRealtime(18.3f); //Entra exactamente en 66.8 de timer, funciono al sumar 0.2 a 18.1 => producto de la resta entre el tiempo 
        letra.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(1.6f); // Destino final perfecto a 68.4.
        letra.gameObject.SetActive(false);



        yield break;
    }
    #endregion


    IEnumerator GhostTest()
    {
        Debug.Log("Se ejecuto la corutina del powerup");
        yield return new WaitForSecondsRealtime(10f);
        Debug.Log("APARECE GHOST");
        InstanciarA1(enemigoGHOST);
        yield break;
    }
}
