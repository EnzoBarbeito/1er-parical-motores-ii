using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoComun : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float fuerza;
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(Vector3.left * fuerza, ForceMode.Impulse);  
    }


}
