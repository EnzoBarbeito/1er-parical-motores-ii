using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropiedadesPowerUPs : MonoBehaviour
{
    [SerializeField] private float velocidadRotacion;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.down * velocidadRotacion * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pared") == true)
        {
            Destroy(gameObject);
        }
    }

}
