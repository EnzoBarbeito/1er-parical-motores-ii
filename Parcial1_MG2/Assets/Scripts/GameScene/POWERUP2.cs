using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POWERUP2 : MonoBehaviour
{
    [SerializeField] private Rigidbody moneda;
    [SerializeField] private Rigidbody STS;
    [SerializeField] private float fuerza;
    [SerializeField] private GameManager Manager;
    void Start()
    {
        STS= GameObject.Find("STS").GetComponent<Rigidbody>();
        moneda = GameObject.Find("Moneda").GetComponent<Rigidbody>();
        Manager = GameObject.Find("-- Game Manager --").GetComponent<GameManager>();
    }


    public void InstanciarMoneda()
    {
        var clone = Instantiate(moneda, moneda.transform.parent.position, moneda.transform.rotation);
        clone.AddForce(Vector3.down * fuerza, ForceMode.Impulse);
    }
    public void InstanciarSTS()
    {
        var clone = Instantiate(STS, STS.transform.position, STS.transform.rotation);
        clone.AddForce(Vector3.down * fuerza, ForceMode.Impulse);
    }


}
