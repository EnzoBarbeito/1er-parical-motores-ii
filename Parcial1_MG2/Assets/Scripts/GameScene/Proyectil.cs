using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{
    [SerializeField] private GameManager Manager;
    void Start()
    {
        GameObject m = GameObject.Find("-- Game Manager --");
        Manager = m.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pared")==true)
        {
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            Manager.Coleccionables+= 1;
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("Vida") == true)
        {
            GDA.instancia.ReproducirSonido("1UP");
            if (Manager.vidasJugador > 0 && Manager.vidasJugador <= 3)
            {
                Manager.vidasJugador = 6;

            }
            if (Manager.vidasJugador > 3 && Manager.vidasJugador <= 6)
            {
                Manager.vidasJugador = 9;

            }
            if (Manager.vidasJugador > 6 && Manager.vidasJugador <= 9)
            {
                Manager.vidasJugador = 9;

            }
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("Enemigo") == true)
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("EnemigoBIG") == true)
        {
            EnemigoBIG a = other.gameObject.GetComponent<EnemigoBIG>();
            a.contadorHits += 1;
            if (a.contadorHits==1)
            {
                Debug.Log("HIT1");
                Destroy(this.gameObject);
                other.gameObject.transform.localScale *= 1.3f;
            }
            if (a.contadorHits==2)
            {
                Debug.Log("HIT2");
                Destroy(this.gameObject);
                other.gameObject.transform.localScale *=1.3f;
            }
            if (a.contadorHits==3)
            {
                Debug.Log("HIT3");
                Destroy(this.gameObject);
                Destroy(other.gameObject);
            }
        }
        if (other.gameObject.CompareTag("EnemigoGHOST") == true)
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            Manager.vidasJugador = 9;
            GDA.instancia.ReproducirSonido("1UP");
        }
    }

   
    
}
