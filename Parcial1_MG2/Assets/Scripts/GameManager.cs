using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    private short decoy;
    [Header("--- HUD ---\n")]
    [SerializeField] internal TMPro.TMP_Text contadorColeccionables;
    [SerializeField] internal TMPro.TMP_Text contadorVidas;
    [SerializeField] internal GameObject[] rankings_gameovers;
    [SerializeField] internal GameObject[] elementosHUD;
    [Header("--- Checks & Variables ---\n")]
    [SerializeField] internal float Coleccionables;
    [SerializeField] internal bool terminoPartida;
    [SerializeField] internal bool level1;
    [SerializeField] internal float vidasJugador;
    [Header("--- Musica ---\n")]
    [SerializeField] private AudioSource[] MusicManager;
    [SerializeField] private float tiempoCancion;
    [Header("--- Instanciadores ---\n")]
    [SerializeField] private InstanciadorPowerUps powerUps;
    [SerializeField] private GameObject instaciadorGameObject;
    [SerializeField] internal bool corutinaActivada;
    [SerializeField] private GeneradorDeNivel GDN;

    private void Awake()
    {
        GDN = GameObject.Find("-- Generador de Nivel --").GetComponent<GeneradorDeNivel>();
        powerUps = GameObject.Find("Instanciador PowerUps").GetComponent<InstanciadorPowerUps>();
        GameObject a = GameObject.Find("-- Music Manager --");
        MusicManager = a.GetComponents<AudioSource>();
        tiempoCancion = 0;
        rankings_gameovers[0].gameObject.SetActive(false);
        rankings_gameovers[1].gameObject.SetActive(false);
        rankings_gameovers[2].gameObject.SetActive(false);
        rankings_gameovers[3].gameObject.SetActive(false);
        rankings_gameovers[4].gameObject.SetActive(false);
        Cursor.visible = false;
        

    }

    void Start()
    {
        GDN.gameObject.transform.position = new Vector3(4.0f, 60.099998474121097f, 137.0f);
        Coleccionables = 0;
        terminoPartida = false;
        corutinaActivada = false;
        level1 = true;
        vidasJugador = 9;
        MusicManager[0].Play();
        MusicManager[1].Play();
        StartCoroutine(tiempoJuego());

    }

    // Update is called once per frame
    void Update()
    {
        MostrarColeccionables();
        Vidas();
        cancionPartida();

    }
    

    private void MostrarColeccionables()
    {
        contadorColeccionables.text = tiempoCancion.ToString("F1");
        contadorVidas.text = vidasJugador.ToString();

    }

    private void Vidas()
    {
        if (vidasJugador == 9)
        {

            MusicManager[0].mute = false; // Vocales
            MusicManager[1].mute = false; // Instrumental
            corutinaActivada = false;
            GameObject a = GameObject.Find("-- Music Manager --");
            if (a.gameObject.GetComponent<AudioHighPassFilter>().enabled == true)
            {
                a.gameObject.GetComponent<AudioHighPassFilter>().enabled = false;
            }
            elementosHUD[0].gameObject.SetActive(true);
            elementosHUD[1].gameObject.SetActive(false);
            elementosHUD[2].gameObject.SetActive(false);
            elementosHUD[6].gameObject.SetActive(true); 
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(true);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(true);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);

        }
        if (vidasJugador == 8)
        {

            corutinaActivada = false;
            GameObject a = GameObject.Find("-- Music Manager --");
            if (a.gameObject.GetComponent<AudioHighPassFilter>().enabled == true)
            {
                a.gameObject.GetComponent<AudioHighPassFilter>().enabled = false;
            }
            elementosHUD[0].gameObject.SetActive(true);
            elementosHUD[1].gameObject.SetActive(false);
            elementosHUD[2].gameObject.SetActive(false);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(true);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(true);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(true);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);

        }
        if (vidasJugador == 7)
        {
            GameObject a = GameObject.Find("-- Music Manager --");
            if (a.gameObject.GetComponent<AudioHighPassFilter>().enabled == true)
            {
                a.gameObject.GetComponent<AudioHighPassFilter>().enabled = false;
            }
            corutinaActivada = false;
            MusicManager[0].mute = false; // Vocales
            MusicManager[1].mute = false;
            elementosHUD[0].gameObject.SetActive(true);
            elementosHUD[1].gameObject.SetActive(false);
            elementosHUD[2].gameObject.SetActive(false);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(true);
            elementosHUD[9].gameObject.SetActive(true);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(true);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);

        }
        if (vidasJugador == 6)
        {
            MusicManager[0].mute = true; // Vocales
            GameObject a = GameObject.Find("-- Music Manager --");
            if (a.gameObject.GetComponent<AudioHighPassFilter>().enabled == true)
            {
                a.gameObject.GetComponent<AudioHighPassFilter>().enabled =false;
            }
            if (corutinaActivada == false)
            {
                StartCoroutine(powerUps.VidaExtra());
            }
            elementosHUD[0].gameObject.SetActive(false);
            elementosHUD[1].gameObject.SetActive(true);
            elementosHUD[2].gameObject.SetActive(false);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(true);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(true);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);
        }
        if (vidasJugador == 5)
        {
            elementosHUD[0].gameObject.SetActive(false);
            elementosHUD[1].gameObject.SetActive(true);
            elementosHUD[2].gameObject.SetActive(false);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(false);
            elementosHUD[10].gameObject.SetActive(true);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(true);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);

        }
        if (vidasJugador == 4)
        {
            GameObject a = GameObject.Find("-- Music Manager --");
            if (a.gameObject.GetComponent<AudioHighPassFilter>().enabled == true)
            {
                a.gameObject.GetComponent<AudioHighPassFilter>().enabled = false;
            }
            elementosHUD[0].gameObject.SetActive(false);
            elementosHUD[1].gameObject.SetActive(true);
            elementosHUD[2].gameObject.SetActive(false);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(false);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(true);
            elementosHUD[12].gameObject.SetActive(true);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);

        }
        if (vidasJugador == 3)
        {
            GameObject a = GameObject.Find("-- Music Manager --");
            a.gameObject.GetComponent<AudioHighPassFilter>().enabled = true;
            AudioHighPassFilter high = a.gameObject.GetComponent<AudioHighPassFilter>();
            high.cutoffFrequency = 1500f;
            if (corutinaActivada == false)
            {
                StartCoroutine(powerUps.VidaExtra());
            }
            elementosHUD[0].gameObject.SetActive(false);
            elementosHUD[1].gameObject.SetActive(false);
            elementosHUD[2].gameObject.SetActive(true);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(false);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(true);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);

        }
        if (vidasJugador == 2)
        {
            elementosHUD[0].gameObject.SetActive(false);
            elementosHUD[1].gameObject.SetActive(false);
            elementosHUD[2].gameObject.SetActive(true);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(false);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(false);
            elementosHUD[13].gameObject.SetActive(true);
            elementosHUD[14].gameObject.SetActive(false);

        }
        if (vidasJugador == 1)
        {
            elementosHUD[0].gameObject.SetActive(false);
            elementosHUD[1].gameObject.SetActive(false);
            elementosHUD[2].gameObject.SetActive(true);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(false);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(false);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(true);

        }
        if (vidasJugador ==0)
        {
            elementosHUD[0].gameObject.SetActive(false);
            elementosHUD[1].gameObject.SetActive(false);
            elementosHUD[2].gameObject.SetActive(true);
            elementosHUD[6].gameObject.SetActive(false);
            elementosHUD[7].gameObject.SetActive(false);
            elementosHUD[8].gameObject.SetActive(false);
            elementosHUD[9].gameObject.SetActive(false);
            elementosHUD[10].gameObject.SetActive(false);
            elementosHUD[11].gameObject.SetActive(false);
            elementosHUD[12].gameObject.SetActive(false);
            elementosHUD[13].gameObject.SetActive(false);
            elementosHUD[14].gameObject.SetActive(false);
            GDA.instancia.ReproducirSonido("DGP");
            StopAllCoroutines();
            Destroy(instaciadorGameObject);
            rankings_gameovers[0].gameObject.SetActive(true);
            //Time.timeScale = 0.1f;

        }
    }

    private void cancionPartida()
    {
        tiempoCancion += 1*Time.deltaTime;
    }

    IEnumerator tiempoJuego()
    {
        rankings_gameovers[0].gameObject.SetActive(false);
        yield return new WaitForSecondsRealtime(255f);
        Victoria();
        yield return new WaitForSecondsRealtime(15f);
        SceneManager.LoadScene(0);
        yield break;
    }

    private void Victoria()
    {
        Debug.Log("Se ejecuto la funcion victoria");
        if (Coleccionables >= 0 && Coleccionables <= 5)
        {
            rankings_gameovers[1].gameObject.SetActive(true);
        }
        if (Coleccionables ==6 && Coleccionables <= 11)
        {
            rankings_gameovers[2].gameObject.SetActive(true);
            GDA.instancia.ReproducirSonido("A");

        }
        if (Coleccionables == 12 && Coleccionables <=17)
        {
            rankings_gameovers[3].gameObject.SetActive(true);
            GDA.instancia.ReproducirSonido("A");

        }
        if (Coleccionables == 18)
        {
            rankings_gameovers[4].gameObject.SetActive(true);
            GDA.instancia.ReproducirSonido("S");

        }
    }
}
